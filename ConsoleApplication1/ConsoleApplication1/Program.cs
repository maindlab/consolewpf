﻿using System;
using System.Runtime.Remoting.Channels;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace ConsoleApplication1
{
    internal class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            var app = new Application();
            var win = new MyWindow { Width = 350, Height = 350};
            var grid = new Grid();
            var text = new TextBox {Text = "my text"};
            grid.Children.Add(text);
            win.Content = grid;
            app.MainWindow = win;
            app.MainWindow.Show();
            Task.Run(async  () =>
            {
                await  Task.Delay(1000);
               app.Dispatcher.Invoke((Action) delegate { app.Shutdown(); });
            });
            app.Run();
        }
        
        private static void Application_Exit(object sender, ExitEventArgs e)
        {
           ((Application)sender).Shutdown();
        }
    }
}